package ar.edu.ciu.objetos2.teoria.interfaces.test;

import ar.edu.ciu.objetos2.teoria.interfaces.model.Loro;
import ar.edu.ciu.objetos2.teoria.interfaces.model.Mascota;
import ar.edu.ciu.objetos2.teoria.interfaces.model.Vendible;
import junit.framework.TestCase;

import java.time.LocalDate;

public class Test extends TestCase {

    public void testLoro() {
        Loro loritoPepe = new Loro();
        loritoPepe.setDescripcion("Loro parlanchin");
        loritoPepe.setHabla(Boolean.TRUE);
        loritoPepe.setEdadLimiteReproductor(5);
        loritoPepe.setFechaDeNacimiento(LocalDate.of(2019, 11, 15));
        loritoPepe.setPeso(400);
        loritoPepe.setAlimento("Alpiste");
        loritoPepe.setCantidadAlimentoDiario(10);

        // lo utilizo como mascota (l método getEdad es de Masctota)
        assertEquals(1, loritoPepe.getEdad());

        // lo utilizao como vendible
        Vendible loritoPepeVendible = (Vendible)loritoPepe;
        assertEquals(6000.0, loritoPepeVendible.getPrecioFinal());
    }

}
