package ar.edu.ciu.objetos2.teoria.interfaces.model;

import java.time.LocalDate;

public class Perro extends Mascota implements Vendible {

    private String raza;
    private LocalDate edadLimiteCachorro;
    private Boolean tienePapeles;

    @Override
    protected Boolean estaEnPesoPromedio() {
        return null;
    }

    @Override
    public Double getPrecioBase() {
        return null;
    }

    @Override
    public Double getPrecioFinal() {
        return null;
    }

    @Override
    public Boolean getEdadOptima() {
        return null;
    }
}
