package ar.edu.ciu.objetos2.factory.method;

import java.util.List;

/*
 *  Product
 */
public interface Taxi {

    public List<Color> getColores();
    public Integer getVelocidadMaxima();
    public Integer getCantidadMaximaDePasajeros();

}
