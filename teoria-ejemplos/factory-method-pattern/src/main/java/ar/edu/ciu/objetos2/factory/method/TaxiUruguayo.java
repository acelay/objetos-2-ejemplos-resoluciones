package ar.edu.ciu.objetos2.factory.method;

import java.util.Arrays;
import java.util.List;

/*
 *  Concrete Product B
 */
public class TaxiUruguayo implements Taxi {

    @Override
    public List<Color> getColores() {
        return Arrays.asList(Color.NEGRO, Color.ROJO);
    }

    @Override
    public Integer getVelocidadMaxima() {
        return Integer.valueOf(60);
    }

    @Override
    public Integer getCantidadMaximaDePasajeros() {
        return Integer.valueOf(4);
    }

}
