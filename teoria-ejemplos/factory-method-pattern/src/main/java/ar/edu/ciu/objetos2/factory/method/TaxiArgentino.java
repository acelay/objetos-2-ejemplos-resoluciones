package ar.edu.ciu.objetos2.factory.method;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 *  Concrete Product A
 */
public class TaxiArgentino implements Taxi {

    @Override
    public List<Color> getColores() {
        return Arrays.asList(Color.NEGRO, Color.AMARILLO);
    }

    @Override
    public Integer getVelocidadMaxima() {
        return Integer.valueOf(80);
    }

    @Override
    public Integer getCantidadMaximaDePasajeros() {
        return Integer.valueOf(3);
    }

}
