package ar.edu.ciu.objetos2.factory.method;

public enum Color {

    NEGRO,
    ROJO,
    AMARILLO;

}
