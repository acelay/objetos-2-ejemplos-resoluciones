package ar.edu.ciu.objetos2.factory.method;

import org.apache.commons.lang3.StringUtils;

/**
 * Link referencia: https://refactoring.guru/es/design-patterns/factory-method
 */
public class Cliente {

    private static final String PAIS = "Uruguay";

    private static Automotriz automotriz;

    private static void config() {
        if (PAIS.equals("Argentina")) {
            automotriz = new AutomotrizArgentina();
        } else if (PAIS.equals("Uruguay")) {
            automotriz = new AutomotrizUruguaya();
        }
    }

    public static void main(String[] args) {
        config();
        Taxi t1 = automotriz.fabricarTaxi();

        System.out.println(t1.getVelocidadMaxima());

        //automotriz.verValoresDePais();

        System.out.println(StringUtils.reverse("!!!opac dirdamaL"));

    }

}
