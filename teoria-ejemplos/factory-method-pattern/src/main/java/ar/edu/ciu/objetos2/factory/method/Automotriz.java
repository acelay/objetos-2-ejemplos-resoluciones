package ar.edu.ciu.objetos2.factory.method;

/*
 * Creator
 */
public abstract class Automotriz {

    public abstract Taxi fabricarTaxi();

    public void verValoresDePais() {
        Taxi taxi = this.fabricarTaxi();
        System.out.println("Velocidad máxima: " + taxi.getVelocidadMaxima());
        System.out.println("Cantidad máxima de pasajeros: " + taxi.getCantidadMaximaDePasajeros());
        System.out.println("Colores:");
        taxi.getColores().forEach( color -> System.out.println("  " + color) );
    }

}
