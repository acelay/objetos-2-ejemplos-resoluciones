package ar.edu.ciu.objetos2.factory.method;

/*
 * Concrete creator A
 */
public class AutomotrizArgentina extends Automotriz {

    @Override
    public Taxi fabricarTaxi() {
        return new TaxiArgentino();
    }

}
