package ar.edu.ciu.objetos2.factory.method;

/*
 * Concrete creator B
 */
public class AutomotrizUruguaya extends Automotriz {

    @Override
    public Taxi fabricarTaxi() {
        return new TaxiUruguayo();
    }

}
