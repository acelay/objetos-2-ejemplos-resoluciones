package ar.com.ejemplo.generics.implements1;

public class Caja<T> {

	// objeto va a ser del tipo T (Generico)
	private T objeto;

	public void setObjeto(T obj) {
		this.objeto = obj;
	}

	public T getObjeto() {
		return objeto;
	}

	public void mostrarTipo() {
		System.out.println("El tipo de T es: " + this.objeto.getClass().getName());
	}

}
