package ar.com.ejemplo.generics.implements1;

public class GenericsEjemplo3 {

	public static void main(String[] args) {
		// Ejemplo con Integer
//		Caja<Integer> caja = new Caja<Integer>();
//		caja.setObjeto(Integer.valueOf(11));
//		//caja.setObjeto("Lamadrid"); // Error de compilacion
//		caja.mostrarTipo();
//		int valor = caja.getObjeto(); // No hay necesidad de cast
//		System.out.println("El valor es: " + valor);
		
		// Ejemplo con Estudiante
		Caja<Estudiante> caja = new Caja<Estudiante>();
		caja.setObjeto(new Estudiante("1098494", "Eda", "Bustamante"));
		caja.mostrarTipo();
		Estudiante valor = caja.getObjeto(); // No hay necesidad de cast
		System.out.println("El valor es: " + valor.getNumeroDeLegajo());
	}

}
