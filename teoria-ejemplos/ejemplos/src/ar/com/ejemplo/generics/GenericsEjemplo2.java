package ar.com.ejemplo.generics;

import java.util.ArrayList;
import java.util.Collection;

import ar.com.ejemplo.collection.Estudiante;

/*
 * Recorrer Collection con Generics
 */
public class GenericsEjemplo2 {

	public static void main(String[] args) {
		Collection<Estudiante> estudiantes = new ArrayList<>();
		Estudiante pepe = new Estudiante("1098494", "Jose", "Sand");
		Estudiante trucho = new Estudiante("1021454", "Ruben", "Moraglio");
		Estudiante lily = new Estudiante("1098546", "Liliana", "Rodriguez");
		Estudiante lau = new Estudiante("1098707", "Laura", "Taccio");
		estudiantes.add(pepe);
		estudiantes.add(trucho);
		estudiantes.add(lily);
		estudiantes.add(lau);
		//estudiantes.add(Integer.valueOf(10)); // Error en compilacion y no en ejecucion.
		estudiantes.stream()
						.forEach( e -> System.out.println(e.getNumeroDeLegajo()));
	}

}
