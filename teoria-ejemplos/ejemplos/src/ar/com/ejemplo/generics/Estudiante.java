package ar.com.ejemplo.generics;

import java.util.Objects;

public class Estudiante {

	// atributos
	private String numeroDeLegajo;
	private String nombre;
	private String apellido;

	// constructor
	public Estudiante(String legajo, String nombre, String apellido) {
		super();
		this.numeroDeLegajo = legajo;
		this.nombre = nombre;
		this.apellido = apellido;
	}

	@Override
	public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Estudiante that = (Estudiante)obj;
        return Objects.equals(this.numeroDeLegajo, that.numeroDeLegajo);
	}

	// gets y sets
	public String getNumeroDeLegajo() {
		return numeroDeLegajo;
	}

	public void setNumeroDeLegajo(String numeroDeLegajo) {
		this.numeroDeLegajo = numeroDeLegajo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

}
