package ar.com.ejemplo.generics.implements2;

public class GenericsEjemplo4 {

	public static void main(String[] args) {
		Ejercito<Guerrero> ejercito = new Ejercito<Guerrero>();
		Maya maya = new Maya();
		Samurai samurai = new Samurai();
		Espartano espartano = new Espartano();
		ejercito.agregarGuerrero(maya);
		ejercito.agregarGuerrero(samurai);
		ejercito.agregarGuerrero(espartano);
		ejercito.mostrarGeurreros();
	}

}
