package ar.com.ejemplo.generics.implements2;

public class Maya extends Guerrero {

	@Override
	public Integer poderDeAtaque() {
		return 89;
	}

	@Override
	public Integer habilidad() {
		return 97;
	}

	@Override
	public String gritoDeGuerra() {
		return "chinga tu madre!";
	}

	@Override
	public String nombre() {
		return "Maya";
	}

}
