package ar.com.ejemplo.generics.implements2;

public class Samurai extends Guerrero {

	@Override
	public Integer poderDeAtaque() {
		return 78;
	}

	@Override
	public Integer habilidad() {
		return 95;
	}

	@Override
	public String gritoDeGuerra() {
		return "Sol naciente las pelo...!!!";
	}

	@Override
	public String nombre() {
		return "Samurai";
	}

}
