package ar.com.ejemplo.generics.implements2;

public abstract class Guerrero {

	public abstract String nombre();
	public abstract Integer poderDeAtaque();
	public abstract Integer habilidad();
	public abstract String gritoDeGuerra();

	@Override
	public String toString() {
		return "Nombre: " + this.nombre() + ", poder ataque: " + this.poderDeAtaque() + ", habilidad: " + this.habilidad() + ", Grito guerra: " + this.gritoDeGuerra();
	}

}
