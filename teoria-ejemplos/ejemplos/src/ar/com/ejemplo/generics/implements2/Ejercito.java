package ar.com.ejemplo.generics.implements2;

import java.util.ArrayList;
import java.util.List;

public class Ejercito<G extends Guerrero> {

	private List<Guerrero> guerreros;

	public Ejercito() {
		super();
		this.guerreros = new ArrayList<Guerrero>();
	}

	public void agregarGuerrero(Guerrero unGuerrero) {
		this.guerreros.add(unGuerrero);
	}

	public void mostrarGeurreros() {
		this.guerreros.stream()
							.forEach( g -> System.out.println(g));
	}

}
