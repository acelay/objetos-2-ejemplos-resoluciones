package ar.com.ejemplo.generics.implements2;

public class Espartano extends Guerrero {

	@Override
	public Integer poderDeAtaque() {
		return 68;
	}

	@Override
	public Integer habilidad() {
		return 71;
	}

	@Override
	public String gritoDeGuerra() {
		return "De Imperio a pedir credito al fmi...muerte a los traidores!!!";
	}

	@Override
	public String nombre() {
		return "Espartano";
	}

}
