package ar.com.ejemplo.generics;

import java.util.ArrayList;
import java.util.Collection;

import ar.com.ejemplo.collection.Estudiante;

/*
 * Recorrer collection sin Generics
 */
public class GenericsEjemplo1 {

	public static void main(String[] args) {
		Collection estudiantes = new ArrayList<>();
		Estudiante pepe = new Estudiante("1098494", "Jose", "Sand");
		Estudiante trucho = new Estudiante("1021454", "Ruben", "Moraglio");
		Estudiante lily = new Estudiante("1098546", "Liliana", "Rodriguez");
		Estudiante lau = new Estudiante("1098707", "Laura", "Taccio");

		estudiantes.add(pepe);
		estudiantes.add(trucho);
		estudiantes.add(lily);
		estudiantes.add(lau);

		// Antes de Java 5 (y eso que no usamos Iterator)
		// Descomentar y ver el error de tipos
//		for (Estudiante estudiante : estudiantes) {
//		}

		// Entonces tengo que recurrir al cast
		Estudiante e = null;
		for (Object estudiante : estudiantes) {
			e = (Estudiante)estudiante;
			System.out.println( e.getNumeroDeLegajo() );
		}

		System.out.println("==================================================");

		// Ademas corro el riesgo de error en tiempo de ejecución
		// Esto compila OK, pero en ejecucion arroja un ClassCastException
//		Integer numero = Integer.valueOf(10);
//		estudiantes.add(numero);
//		for (Object estudiante : estudiantes) {
//			e = (Estudiante)estudiante;
//			System.out.println( e.getNumeroDeLegajo() );
//		}
		
		// Ademas corro el riesgo de error en tiempo de ejecución, ahora con Stream
		// Esto compila OK, pero en ejecucion arroja un ClassCastException
		Integer numero = Integer.valueOf(10);
		estudiantes.add(numero);
		estudiantes.stream()
						.forEach( x -> System.out.println( ((Estudiante)x).getNumeroDeLegajo() ));
	}

}
