package ar.com.ejemplo.generics.implements3;

public class QuickSort<T extends Comparable<T>> {

	private int particion(T arreglo[], int izquierda, int derecha) {
        T pivote = arreglo[izquierda];
        // ciclo infinito
        while (true) {
            while (arreglo[izquierda].compareTo(pivote)<0) {
                izquierda++;
            }
            while (arreglo[derecha].compareTo(pivote)>0) {
                derecha--;
            }
            if (izquierda >= derecha) {
                return derecha;
            } else {
            	// nota: yo sé que el else no hace falta por el return de arriba, pero así el algoritmo es más claro
                T temporal = arreglo[izquierda];
                arreglo[izquierda] = arreglo[derecha];
                arreglo[derecha] = temporal;
                izquierda++;
                derecha--;
            }
            // el while se repite hasta que izquierda >= derecha
        }
    }

	public void quicksort(T arreglo[], int izquierda, int derecha) {
	    if (izquierda < derecha) {
	        int indiceParticion = particion(arreglo, izquierda, derecha);
	        quicksort(arreglo, izquierda, indiceParticion);
	        quicksort(arreglo, indiceParticion + 1, derecha);
	    }
	}
	
}
