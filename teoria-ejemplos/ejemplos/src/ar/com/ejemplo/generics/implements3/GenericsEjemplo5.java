package ar.com.ejemplo.generics.implements3;

import java.util.Arrays;

public class GenericsEjemplo5 {

	public static void main(String[] args) {
		
		System.out.println("=====>>>>> Integer <<<<<======");
		QuickSort<Integer> qsInteger = new QuickSort<Integer>();
		Integer numeros[] = {1, 9, 23, 4, 55, 100, 1, 1, 23};
        System.out.println("Antes de QS: " + Arrays.toString(numeros));
        qsInteger.quicksort(numeros, 0, numeros.length - 1);
        System.out.println("Después de QS: " + Arrays.toString(numeros));
        
        System.out.println("=====>>>>> String <<<<<======");
		QuickSort<String> qsString = new QuickSort<String>();
		String palabras[] = {"Lamadrid", "River", "El Porvenir", "Deportivo Español", "Independiente", "San Lorenzo"};
        System.out.println("Antes de QS: " + Arrays.toString(palabras));
        qsString.quicksort(palabras, 0, palabras.length - 1);
        System.out.println("Después de QS: " + Arrays.toString(palabras));
	}

}
