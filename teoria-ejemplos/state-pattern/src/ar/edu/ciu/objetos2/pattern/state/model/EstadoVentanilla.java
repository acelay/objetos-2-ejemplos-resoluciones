package ar.edu.ciu.objetos2.pattern.state.model;

public abstract class EstadoVentanilla {

  public abstract void atender(Persona persona);
}
