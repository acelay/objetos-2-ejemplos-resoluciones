package ar.edu.ciu.objetos2.exceptions;

public class MyException extends Exception {

    public MyException(String mensaje) {
        super(mensaje);
    }

}
