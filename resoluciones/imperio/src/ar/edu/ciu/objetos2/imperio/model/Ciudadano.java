package ar.edu.ciu.objetos2.imperio.model;

public abstract class Ciudadano {

    protected String nombre;
    protected String legajo;

    public Ciudadano(String nombre, String legajo) {
        super();
        this.nombre = nombre;
        this.legajo = legajo;
    }

    public String getLegajo() {
        return this.legajo;
    }

    @Override
    public String toString() {
        return this.legajo + " - " + this.nombre;
    }

}
