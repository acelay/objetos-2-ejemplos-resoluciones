package ar.edu.ciu.objetos2.imperio.model;

public class Aldeano extends Ciudadano implements Militar {

    private Boolean estaCansado;
    public Aldeano(String nombre, String legajo, Boolean estaCansado) {
        super(nombre, legajo);
        this.estaCansado = estaCansado;
    }

    @Override
    public Double poderDeGenerarDanio() {
        return 0.1 + (this.estaCansado?0.00:0.1);
    }
}
