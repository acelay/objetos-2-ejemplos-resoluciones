package ar.edu.ciu.objetos2.imperio.model;

import java.util.ArrayList;
import java.util.List;

public class Imperio {

    private List<Gobernante> gobernantes;
    private List<Aldeano> aldeanos;
    private List<Milicia> milicias;

    public Imperio() {
        super();
        this.gobernantes = new ArrayList<>();
        this.aldeanos = new ArrayList<>();
        this.milicias = new ArrayList<>();
    }

    public void agregar(Gobernante gobernante) {
        if (!this.existeLegajo(gobernante.getLegajo())) {
            this.gobernantes.add(gobernante);
        } else {
            System.out.println("Legajo: " + gobernante.getLegajo() + " existente.");
        }
    }

    public void agregar(Aldeano aldeano) {
        if (!this.existeLegajo(aldeano.getLegajo())) {
            this.aldeanos.add(aldeano);
        } else {
            System.out.println("Legajo: " + aldeano.getLegajo() + " existente.");
        }
    }

    public void agregar(Milicia militar) {
        if (!this.existeLegajo(militar.getLegajo())) {
            this.milicias.add(militar);
        } else {
            System.out.println("Legajo: " + militar.getLegajo() + " existente.");
        }
    }

    private boolean existeLegajo(String legajo) {
        return this.getCiudadanos().stream()
                                        .map(Ciudadano::getLegajo)
                                        .anyMatch(leg -> leg.equals(legajo));
    }

    public List<Ciudadano> getCiudadanos() {
        List<Ciudadano> ciudadanos = new ArrayList<>();
        ciudadanos.addAll(this.gobernantes);
        ciudadanos.addAll(this.aldeanos);
        ciudadanos.addAll(this.milicias);
        return ciudadanos;
    }

    public Integer getCantidadDeCiudadanos() {
        return this.getCiudadanos().size();
    }

    public Double getPoderDeAtaque() {
        return this.milicias.stream()
                                    .mapToDouble(Milicia::poderDeGenerarDanio)
                                    .sum();
    }

    public Double getPoderDeDefensa() {
        List<Militar> militares = new ArrayList<>();
        militares.addAll(this.milicias);
        militares.addAll(this.aldeanos);
        return militares.stream()
                .mapToDouble(Militar::poderDeGenerarDanio)
                .sum();
    }

}
