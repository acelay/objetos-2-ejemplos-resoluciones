package ar.edu.ciu.objetos2.imperio.model;

public interface Militar {

    public Double poderDeGenerarDanio();

}
