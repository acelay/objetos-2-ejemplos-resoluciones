package ar.edu.ciu.objetos2.imperio.model;

import java.util.ArrayList;
import java.util.List;

public class Milicia extends Ciudadano implements Militar {

    private List<Arma> armas;
    public Milicia(String nombre, String legajo) {
        super(nombre, legajo);
    }

    @Override
    public Double poderDeGenerarDanio() {
        return 0.4 + this.armas.size()*0.1;
    }

    public void add(Arma arma) {
        if (this.armas==null)
            this.armas = new ArrayList<>();
        this.armas.add(arma);
    }

}
