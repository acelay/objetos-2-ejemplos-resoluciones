package ar.edu.ciu.objetos2.oktubrefest.model;

public class CervezaNegra extends Marca {

    private static Double graduacionReglamentaria = 0.06;

    public CervezaNegra(String descripcion, Integer lupuloPorLitro, Pais paisOrigen) {
        super(descripcion, lupuloPorLitro, paisOrigen);
    }

    @Override
    public Double getGraduacion() {
        return (this.lupuloPorLitro*2) * graduacionReglamentaria;
    }

    public static Double getGraduacionReglamentaria() {
        return graduacionReglamentaria;
    }

    public static void setGraduacionReglamentaria(Double graduacionReglamentaria) {
        CervezaNegra.graduacionReglamentaria = graduacionReglamentaria;
    }

}
