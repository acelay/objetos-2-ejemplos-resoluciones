package ar.edu.ciu.objetos2.oktubrefest.model;

public class Jarra {

    // litros
    private Double capacidad;
    private Carpa carpa;

    public Jarra(Double capacidad, Carpa carpa) {
        super();
        this.capacidad = capacidad;
        this.carpa = carpa;
    }

    public Double getAporteAlcohol() {
        return this.capacidad * this.carpa.getMarca().getGraduacion();
    }

    public Double getCapacidad() {
        return this.capacidad;
    }

    public Carpa getCarpa() {
        return this.carpa;
    }

    public Marca getMarca() {
        return this.carpa.getMarca();
    }

}
