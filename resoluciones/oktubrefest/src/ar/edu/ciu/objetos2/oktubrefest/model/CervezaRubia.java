package ar.edu.ciu.objetos2.oktubrefest.model;

public class CervezaRubia extends Marca {

    private Double graduacion;

    public CervezaRubia(String descripcion, Integer lupuloPorLitro, Pais paisOrigen, Double graduacion) {
        super(descripcion, lupuloPorLitro, paisOrigen);
        this.graduacion = graduacion;
    }

    @Override
    public Double getGraduacion() {
        return this.graduacion;
    }

}
