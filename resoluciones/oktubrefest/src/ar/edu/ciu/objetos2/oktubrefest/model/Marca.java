package ar.edu.ciu.objetos2.oktubrefest.model;

public abstract class Marca {

    protected String descripcion;
    // gramos
    protected Integer lupuloPorLitro;
    protected Pais paisOrigen;

    protected Marca(String descripcion, Integer lupuloPorLitro, Pais paisOrigen) {
        super();
        this.descripcion = descripcion;
        this.lupuloPorLitro = lupuloPorLitro;
        this.paisOrigen = paisOrigen;
    }

    public abstract Double getGraduacion();

    public Pais getPaisOrigen() {
        return paisOrigen;
    }

    public Integer getLupuloPorLitro() {
        return lupuloPorLitro;
    }

}
