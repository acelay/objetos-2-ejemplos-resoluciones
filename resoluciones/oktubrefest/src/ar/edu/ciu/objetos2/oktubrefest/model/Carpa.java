package ar.edu.ciu.objetos2.oktubrefest.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class Carpa {

    private String descripcion;
    private Integer capacidadMaximaDePersonas;
    private Boolean tieneBandaMusical;
    private Marca marca;
    private Integer cantidadActualDePersonas;
    private Collection<Persona> personasQueEstuvieron;

    public Carpa(String descripcion, Marca marca, Integer capacidad) {
        super();
        this.descripcion = descripcion;
        this.marca = marca;
        this.capacidadMaximaDePersonas = capacidad;
        this.cantidadActualDePersonas = Integer.valueOf(0);
        this.personasQueEstuvieron = new ArrayList<>();
        this.tieneBandaMusical = Boolean.FALSE;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCapacidadMaximaDePersonas() {
        return capacidadMaximaDePersonas;
    }

    public void setCapacidadMaximaDePersonas(Integer capacidadMaximaDePersonas) {
        this.capacidadMaximaDePersonas = capacidadMaximaDePersonas;
    }

    public Boolean getTieneBandaMusical() {
        return tieneBandaMusical;
    }

    public void setTieneBandaMusical(Boolean tieneBandaMusical) {
        this.tieneBandaMusical = tieneBandaMusical;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Integer getCantidadActualDePersonas() {
        return cantidadActualDePersonas;
    }

    public void setCantidadActualDePersonas(Integer cantidadActualDePersonas) {
        this.cantidadActualDePersonas = cantidadActualDePersonas;
    }

    public Collection<Persona> getPersonasQueEstuvieron() {
        return personasQueEstuvieron;
    }

    public void setPersonasQueEstuvieron(Collection<Persona> personasQueEstuvieron) {
        this.personasQueEstuvieron = personasQueEstuvieron;
    }

    public boolean permiteEntrarA(Persona persona) {
        return this.cantidadActualDePersonas<this.capacidadMaximaDePersonas && !persona.estaEbria();
    }

    public void admitir(Persona persona) {
        if (persona.puedeEntrarEn(this)) {
            this.personasQueEstuvieron.add(persona);
            this.cantidadActualDePersonas++;
        } else {
            System.out.println("La persona no puede entar a la carpa");
        }
    }

    public Collection<Persona> getEbriosEmpedernidos() {
        return personasQueEstuvieron.stream()
                                        .filter(Persona::esEbrioEmpedernido)
                                        .collect(Collectors.toList());
    }

    public int cantidadEbriosEmpedernidos(){
        return this.getEbriosEmpedernidos().size();
    }

}
