package ar.edu.ciu.objetos2.oktubrefest.model;

import ar.edu.ciu.objetos2.oktubrefest.Utilidades;

public class Aleman extends Persona {

    public Aleman(Double peso, Boolean escuchaMusica, Integer nivelDeAguante) {
        super(peso, escuchaMusica, nivelDeAguante);
        this.pais = new Pais("Alemania");
    }

    @Override
    public boolean leGustaMarca(Marca marca) {
        return true;
    }

    @Override
    public boolean quiereEntrarEn(Carpa carpa){
        return super.quiereEntrarEn(carpa) && Utilidades.esPar(carpa.getCantidadActualDePersonas());
    }

}
