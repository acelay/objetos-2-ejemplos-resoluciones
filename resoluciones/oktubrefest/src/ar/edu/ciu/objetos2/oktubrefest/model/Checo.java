package ar.edu.ciu.objetos2.oktubrefest.model;

public class Checo extends  Persona {

    public Checo(Double peso, Boolean escuchaMusica, Integer nivelDeAguante) {
        super(peso, escuchaMusica, nivelDeAguante);
        this.pais = new Pais("Checo");
    }

    @Override
    public boolean leGustaMarca(Marca marca) {
        return marca.getGraduacion() > 8;
    }

}
