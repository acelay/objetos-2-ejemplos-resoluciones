package ar.edu.ciu.objetos2.oktubrefest.model;

public class CervezaRoja extends CervezaNegra {

    public CervezaRoja(String descripcion, Integer lupuloPorLitro, Pais paisOrigen) {
        super(descripcion, lupuloPorLitro, paisOrigen);
    }

    @Override
    public Double getGraduacion() {
        return super.getGraduacion() * 1.25;
    }

}
