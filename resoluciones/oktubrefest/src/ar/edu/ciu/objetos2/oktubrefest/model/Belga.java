package ar.edu.ciu.objetos2.oktubrefest.model;

public class Belga extends Persona {

    public Belga(Double peso, Boolean escuchaMusica, Integer nivelDeAguante) {
        super(peso, escuchaMusica, nivelDeAguante);
        this.pais = new Pais("Belgica");
    }

    @Override
    public boolean leGustaMarca(Marca marca) {
        return marca.getLupuloPorLitro() > 4;
    }

}
