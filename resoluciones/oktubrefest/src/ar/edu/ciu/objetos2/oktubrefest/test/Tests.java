package ar.edu.ciu.objetos2.oktubrefest.test;

import ar.edu.ciu.objetos2.oktubrefest.model.*;
import junit.framework.TestCase;

public class Tests extends TestCase {

    public void testItem1() {
        Pais alemania = new Pais("Alemania");
        Marca hofbrau = new CervezaRoja("Hofbrau", 5, alemania);
        Carpa carpa = new Carpa("Traka traka", hofbrau,8);
        Jarra jarraComunista = new Jarra(0.5, carpa);
        assertEquals(Double.valueOf(0.375), jarraComunista.getAporteAlcohol());
    }

    public void testItem2() {
        Pais alemania = new Pais("Alemania");
        Marca hofbrau = new CervezaRoja("Hofbrau", 5, alemania);
        Carpa carpa = new Carpa("Traka traka", hofbrau,8);
        Jarra jarraComunista = new Jarra(0.5, carpa);

        Pais argentina = new Pais("Argentina");
        Marca palermo = new CervezaRubia("Palermo", 4, argentina, 0.52);
        Carpa carpa2 = new Carpa("Traka traka", palermo,8);
        Jarra jarraPlebella = new Jarra(1.0, carpa2);

        assertEquals(Double.valueOf(0.52), jarraPlebella.getAporteAlcohol());
    }

    public void testItem3() {
        Pais argentina = new Pais("Argentina");
        Marca palermo = new CervezaRubia("Palermo", 4, argentina, 0.52);
        Carpa carpa = new Carpa("Traka traka", palermo,8);
        Jarra jarraPlebella1 = new Jarra(1.0, carpa);

        Persona obreroParasito = new Belga(71.0, Boolean.FALSE, 15);
        obreroParasito.comprar(jarraPlebella1);

        assertTrue(obreroParasito.estaEbria());
    }

    public void testItem4() {
        Persona klinsmann = new Aleman(83.0, Boolean.FALSE, 3);
        Pais belgica = new Pais("Belgica");
        CervezaNegra cervezaNegra = new CervezaNegra("Morochita",4, belgica);
        Carpa carpa = new Carpa("Traka traka", cervezaNegra,8);

        assertTrue(klinsmann.quiereEntrarEn(carpa));
    }

    public void testPunto5() {
        Aleman koenn = new Aleman(66.0, Boolean.FALSE,6);
        Pais alemania = new Pais("Alemania");
        Marca hofbrau = new CervezaRoja("Hofbrau", 5, alemania);
        Carpa carpa = new Carpa("Traka traka", hofbrau,8);
        Jarra jarra1 = new Jarra(0.5, carpa);
        koenn.comprar(jarra1);
        assertFalse(carpa.permiteEntrarA(koenn));
    }

    public void testPunto6() {
        Pais alemania = new Pais("Alemania");
        Marca hofbrau = new CervezaRoja("Hofbrau", 5, alemania);
        Carpa c = new Carpa("Lamadrid", hofbrau, 20);
        Persona p = new Belga(80.00, false, 5);
        //TODO resta preparar el caso contexto del test
        p.puedeEntrarEn(c);
    }

    public void testPunto7() {
        Pais alemania = new Pais("Alemania");
        Marca hofbrau = new CervezaRoja("Hofbrau", 5, alemania);
        Carpa c = new Carpa("Lamadrid", hofbrau, 20);
        Persona p = new Belga(80.00, false, 5);
        //TODO resta preparar el caso contexto del test
        c.admitir(p);
    }

    public void testPunto8() {
        Pais alemania = new Pais("Alemania");
        Marca hofbrau = new CervezaRoja("Hofbrau", 5, alemania);
        Carpa c = new Carpa("Lamadrid", hofbrau, 20);
        //TODO resta preparar el caso contexto del test
        c.cantidadEbriosEmpedernidos();

    }

    public void testPunto9() {
        Pais alemania = new Pais("Alemania");
        Marca hofbrau = new CervezaRoja("Hofbrau", 5, alemania);
        Carpa c = new Carpa("Lamadrid", hofbrau, 20);
        Persona p = new Belga(80.00, false, 5);
        //TODO resta preparar el caso contexto del test
        p.esPatriota();
    }

    public void testPunto10() {
        Pais alemania = new Pais("Alemania");
        Marca hofbrau = new CervezaRoja("Hofbrau", 5, alemania);
        Carpa c = new Carpa("Lamadrid", hofbrau, 20);
        Persona p = new Belga(80.00, false, 5);
        //TODO resta preparar el caso contexto del test
        p.getCarpasDondeCompro();
    }

    public void testPunto11() {
        Pais alemania = new Pais("Alemania");
        Marca hofbrau = new CervezaRoja("Hofbrau", 5, alemania);
        Carpa c = new Carpa("Lamadrid", hofbrau, 20);
        Persona p1 = new Belga(80.00, false, 5);
        Persona p2 = new Belga(82.00, false, 7);
        //TODO resta preparar el caso contexto del test
        p1.fueJuntoCon(p2);
    }

}
