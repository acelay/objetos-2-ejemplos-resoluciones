package ar.edu.ciu.objetos2.oktubrefest.model;

public class Pais {

    private String descripcion;

    public Pais(String descripcion) {
        super();
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object obj) {
        return ((Pais)obj).getDescripcion().equals(this.descripcion);
    }

    public String getDescripcion() {
        return descripcion;
    }

}
