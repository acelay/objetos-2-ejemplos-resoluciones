package ar.edu.ciu.objetos2.oktubrefest.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public abstract class Persona {

    // kilogramos
    protected Double peso;
    protected Boolean escuchaMusica;
    protected Integer nivelDeAguante;
    protected Collection<Jarra> jarrasCompradas;
    protected Pais pais;

    public Persona(Double peso, Boolean escuchaMusica, Integer nivelDeAguante) {
        super();
        this.peso = peso;
        this.escuchaMusica = escuchaMusica;
        this.nivelDeAguante = nivelDeAguante;
        this.jarrasCompradas = new ArrayList<Jarra>();
    }

    public abstract boolean leGustaMarca(Marca marca);

    public void comprar(Jarra unaJarra) {
        this.jarrasCompradas.add(unaJarra);
    }

    public Double getCantidadDeAlcoholIngerido() {
        return this.jarrasCompradas.stream()
                                        .mapToDouble(Jarra::getAporteAlcohol)
                                        .sum();
    }

    public Boolean estaEbria() {
        return (this.getCantidadDeAlcoholIngerido()*this.peso)>this.nivelDeAguante;
    }

    public boolean quiereEntrarEn(Carpa carpa) {
        return this.leGustaMarca(carpa.getMarca()) && (this.escuchaMusica.equals(carpa.getTieneBandaMusical()));
    }

    public boolean puedeEntrarEn(Carpa carpa) {
        return this.quiereEntrarEn(carpa) && carpa.permiteEntrarA(this);
    }

    public boolean esEbrioEmpedernido(){
        return this.estaEbria() && this.jarrasCompradas.stream()
                                                            .allMatch(j -> j.getCapacidad() > 1);
    }

    public boolean esPatriota() {
        return this.jarrasCompradas.stream()
                                        .allMatch(j -> j.getMarca().getPaisOrigen().equals(pais));
    }

    public Collection<Carpa> getCarpasDondeCompro() {
        return this.jarrasCompradas.stream()
                                        .map(Jarra::getCarpa)
                                        .distinct()
                                        .collect(Collectors.toList());
    }

    public Boolean fueJuntoCon(Persona otraPersona) {
        return this.getCarpasDondeCompro().size()==otraPersona.getCarpasDondeCompro().size() &&
                    this.getCarpasDondeCompro().containsAll(otraPersona.getCarpasDondeCompro());
    }

}
