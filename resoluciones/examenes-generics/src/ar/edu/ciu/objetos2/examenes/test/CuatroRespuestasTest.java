package ar.edu.ciu.objetos2.examenes.test;

import ar.edu.ciu.objetos2.examenes.model.ResolucionDeExamen;
import ar.edu.ciu.objetos2.examenes.model.pregunta.PreguntaMultipleChoice;
import ar.edu.ciu.objetos2.examenes.model.pregunta.PreguntaNumerica;
import ar.edu.ciu.objetos2.examenes.model.respuesta.RespuestaMultipleChoice;
import ar.edu.ciu.objetos2.examenes.model.respuesta.RespuestaNumerica;
import junit.framework.TestCase;

public class CuatroRespuestasTest extends TestCase {

	public void testPuntaje() {
		ResolucionDeExamen reso = new ResolucionDeExamen();

		PreguntaNumerica losTrescientos = new PreguntaNumerica(12, 300);
		losTrescientos.setRangoAproximado(250, 320, 3);

		reso.addToRespuestas(new RespuestaMultipleChoice(new PreguntaMultipleChoice(5, "c")));
		reso.addToRespuestas(new RespuestaMultipleChoice(new PreguntaMultipleChoice(10, "a")));
		reso.addToRespuestas(new RespuestaMultipleChoice(new PreguntaMultipleChoice(20, "d")));
		reso.addToRespuestas(new RespuestaNumerica(losTrescientos));
		
		reso.getRespuestas().get(0).setValor("c");
		reso.getRespuestas().get(1).setValor("c");
		reso.getRespuestas().get(2).setValor("d");
		reso.getRespuestas().get(3).setValor("280");
		
		assertEquals(28, reso.getPuntajeObtenido());
	}

}
