package ar.edu.ciu.objetos2.civilization.model;

import java.util.ArrayList;
import java.util.List;

public class Tecnologia {

	private String nombre;
	private List<Tecnologia> requisitos;

	public Tecnologia(String nombre) {
		super();
		this.nombre = nombre;
	}

	public void agregarRequisito(Tecnologia t) {
		if (this.requisitos==null) {
			this.requisitos = new ArrayList<Tecnologia>();
		}
		this.requisitos.add(t);
	}

	public List<Tecnologia> getRequisitos() {
		return requisitos;
	}

	public String getNombre() {
		return nombre;
	}

	public boolean cumpleRequisitos(List<Tecnologia> tecnologiasExistentes) {
		if (this.requisitos==null) {
			return true;
		}
		return tecnologiasExistentes.containsAll(this.requisitos);
	}

}
