package ar.edu.ciu.objetos2.civilization.model.humor;

import ar.edu.ciu.objetos2.civilization.model.Ciudad;

public class Pacifista extends Humor {

	@Override
	public Integer disconformidad(Ciudad ciudad) {
		return ciudad.getHabitantes()/15000 + ciudad.getCantidadDeUnidadesMilitares();
	}

}
