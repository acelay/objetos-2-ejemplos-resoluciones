package ar.edu.ciu.objetos2.civilization.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Imperio {

	private List<Tecnologia> tecnologias;
	private List<Ciudad> sedes;
	private Tesoro tesoro;

	public Imperio() {
		super();
		this.tecnologias = new ArrayList<Tecnologia>();
	}

	public void agregarSede(Ciudad ciudad) {
		if (this.sedes==null) {
			this.sedes = new ArrayList<Ciudad>();
		}
		this.sedes.add(ciudad);
	}

	public List<Ciudad> getCiudadesPorCultura() {
		return this.sedes.stream()
							.sorted(Comparator.comparing(Ciudad::getCulturaQueIrradianEdificios).reversed())
							.collect(Collectors.toList());
	}

	public Tecnologia incorporarTecnologia(Tecnologia nuevaTecnologia) {
		if (nuevaTecnologia.cumpleRequisitos(this.tecnologias)) {
			this.tecnologias.add(nuevaTecnologia);
			return nuevaTecnologia;
		}
		return null;
	}

	public Integer ingresosPorTurno() {
		return this.sedes.stream()
							.mapToInt(Ciudad::ingresosPorTurno)
							.sum();
	}

	public Integer egresosPorTurno() {
		return this.sedes.stream()
							.mapToInt(Ciudad::egresosPorTurno)
							.sum();
	}

	public Integer potenciaPorTurno() {
		return this.sedes.stream()
				.mapToInt(Ciudad::potenciaPorTurno)
				.sum();
	}

	public void evolucion() {
		this.evolucionSedes();
		this.restarMantenimientoEdificios();
		this.sumarProduccionEdificios();
		this.crearUnidadesMilitares();
	}

	private void crearUnidadesMilitares() {
		this.sedes.stream()
						.forEach(Ciudad::crearUnidadesMilitares);
	}

	private void sumarProduccionEdificios() {
		this.tesoro.sumarPepines(this.getProduccionEdificios());
	}

	private Integer getProduccionEdificios() {
		return this.sedes.stream()
							.mapToInt(Ciudad::getProduccionEdificios)
							.sum();
	}

	private void restarMantenimientoEdificios() {
		this.tesoro.restarPepines(this.getCostoMantenimientoEdificios());
	}

	private Integer getCostoMantenimientoEdificios() {
		return this.sedes.stream()
							.mapToInt(Ciudad::getCostoMantenimientoEdificios)
							.sum();
	}

	private void evolucionSedes() {
		this.sedes.stream()
					.filter(Ciudad::esFeliz)
					.forEach(Ciudad::aumentarPoblacion);
	}

	public List<Tecnologia> getTecnologias() {
		return tecnologias;
	}

	public List<Ciudad> getSedes() {
		return sedes;
	}

	public Tesoro getTesoro() {
		return tesoro;
	}

	public void setTesoro(Tesoro tesoro) {
		this.tesoro = tesoro;
	}

}
