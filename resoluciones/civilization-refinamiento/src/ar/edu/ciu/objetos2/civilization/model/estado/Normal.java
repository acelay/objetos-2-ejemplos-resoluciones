package ar.edu.ciu.objetos2.civilization.model.estado;

import ar.edu.ciu.objetos2.civilization.model.Ciudad;
import ar.edu.ciu.objetos2.civilization.model.edificio.Edificio;

public class Normal extends Estado {

	@Override
	public Integer getPorcentajeAumentoPoblacion(Ciudad ciudad) {
		return 5;
	}

	@Override
	public Integer getCostoDeMantenimiento(Edificio edificio) {
		return edificio.getCostoMantenimiento();
	}

}
