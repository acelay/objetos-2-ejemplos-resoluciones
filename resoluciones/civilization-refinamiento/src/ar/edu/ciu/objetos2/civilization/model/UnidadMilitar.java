package ar.edu.ciu.objetos2.civilization.model;

public class UnidadMilitar {

	private Integer potencia;

	public UnidadMilitar(Integer potencia) {
		super();
		this.potencia = potencia;
	}

	public Integer getPotencia() {
		return potencia;
	}

}
