package ar.edu.ciu.objetos2.civilization.model.edificio;

import ar.edu.ciu.objetos2.civilization.model.UnidadMilitar;

public class EdificioMilitar extends Edificio {

	private Integer potenciaDeUnidadMilitar;

	public EdificioMilitar(Integer costoConstruccion, Integer costoMantenimiento, Integer potenciaDeUnidadMilitar) {
		super(costoConstruccion, costoMantenimiento);
		this.potenciaDeUnidadMilitar = potenciaDeUnidadMilitar;
	}

	@Override
	public Integer getCulturaIrradiada() {
		return 0;
	}

	@Override
	public Integer getTranquilidadAportada() {
		return 1;
	}

	public void agregarUnidadMilitar() {
		this.ciudad.agregarUnidadMilitar(this.crearUnidadMilitar());
	}

	public UnidadMilitar crearUnidadMilitar() {
		return new UnidadMilitar(this.potenciaDeUnidadMilitar);
	}

	@Override
	public Integer getValor() {
		return this.getPotencia();
	}

	@Override
	public Integer getPotencia() {
		return this.potenciaDeUnidadMilitar;
	}

	public Boolean esMilitar() {
		return Boolean.TRUE;
	}

	@Override
	public Boolean esCultural() {
		return Boolean.FALSE;
	}

}
