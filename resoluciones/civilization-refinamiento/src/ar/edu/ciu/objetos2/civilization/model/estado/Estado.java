package ar.edu.ciu.objetos2.civilization.model.estado;

import ar.edu.ciu.objetos2.civilization.model.Ciudad;
import ar.edu.ciu.objetos2.civilization.model.edificio.Edificio;

public abstract class Estado {

	public abstract Integer getPorcentajeAumentoPoblacion(Ciudad ciudad);

	public abstract Integer getCostoDeMantenimiento(Edificio edificio);

}
