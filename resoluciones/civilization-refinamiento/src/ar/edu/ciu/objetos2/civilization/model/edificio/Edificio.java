package ar.edu.ciu.objetos2.civilization.model.edificio;

import ar.edu.ciu.objetos2.civilization.model.Ciudad;

public abstract class Edificio {

	protected Ciudad ciudad;
	protected Integer costoConstruccion;
	protected Integer costoMantenimiento;

	public Edificio(Integer costoConstruccion, Integer costoMantenimiento) {
		super();
		this.costoConstruccion = costoConstruccion;
		this.costoMantenimiento = costoMantenimiento;
	}

	public abstract Integer getCulturaIrradiada();
	public abstract Integer getTranquilidadAportada();
	public abstract Integer getValor();
	public abstract Boolean esCultural();

	public Integer getPepinesPorTurno() {
		return 0;
	}

	public Integer getPotencia() {
		return 0;
	}

	public Boolean esMilitar() {
		return Boolean.FALSE;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public Ciudad getCiudad() {
		return ciudad;
	}

	public Integer getCostoConstruccion() {
		return costoConstruccion;
	}

	public Integer getCostoMantenimiento() {
		return this.ciudad.getEstado().getCostoDeMantenimiento(this);
	}

}
