package ar.edu.ciu.objetos2.civilization.model;

public class Juego {

	private static Juego instance;

	private Integer factorTranquilidad;

	private Juego() {
		super();
		this.factorTranquilidad = 2;
	}

	public static Juego getInstance() {
		if (instance==null) {
			instance = new Juego();
		}
		return instance;
	}

	public Integer getFactorTranquilidad() {
		return factorTranquilidad;
	}

}
