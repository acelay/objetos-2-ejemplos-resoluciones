package ar.edu.ciu.objetos2.ascenso;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class test {

    @Test
    public void test1() {
        Partido p1 = new Partido("Dep. Laferrere", "Lamadrid");
        Emisora prensaLamadrid = new Emisora("Prensa Lamadrid");
        prensaLamadrid.setPartidoTransmitido(p1);

        Partido p2 = new Partido("Sol de Mayo", "Villa Mitre");
        Emisora futbolSur = new Emisora("Fútbol Sur");
        futbolSur.setPartidoTransmitido(p2);

        Partido p3 = new Partido("Liniers", "CentralBallester");
        Emisora pelota = new Emisora("Pelota");
        pelota.setPartidoTransmitido(p3);

        Manager manager = new Manager();
        manager.agregar(prensaLamadrid);
        manager.agregar(futbolSur);
        manager.agregar(pelota);

        prensaLamadrid.generarNotificacion(61, "Gol de Lamadrid!!! Lama se impone por 1 a 0!");
        Assertions.assertTrue(true);
    }

}
