package ar.edu.ciu.objetos2.remiseria.model.interior;

public abstract class Interior {

	// atributos
	protected Integer capacidad;
	protected Integer peso;

	// constructor
	public Interior(Integer capacidad, Integer peso) {
		super();
		this.capacidad = capacidad;
		this.peso = peso;
	}

	// metodos
	public Integer getCapacidad() {
		return capacidad;
	}

	public Integer getPeso() {
		return peso;
	}

}
