package ar.edu.ciu.objetos2.remiseria.model;

public final class StandardGas extends Standard {

	// atributos
	private Boolean adicional;

	// constructor
	public StandardGas(Boolean adicional) {
		super(4, 120, 1200, "azul");
		this.adicional = adicional;
	}

	// metodos instancia
	@Override
	public Integer getCapacidad() {
		return (!this.adicional) ? this.capacidad : this.capacidad-1;
	}

	@Override
	public Integer getVelocidadMaxima() {
		return (this.adicional) ? this.velocidadMaxima : this.velocidadMaxima-10;
	}

	@Override
	public Integer getPeso() {
		return (!this.adicional) ? this.peso : this.peso+150;
	}

	@Override
	public String getColor() {
		return this.color;
	}

}
