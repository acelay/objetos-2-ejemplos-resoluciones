package ar.edu.ciu.objetos2.remiseria.model;

public final class ChevroletCorsa extends Standard {

	// constructor
	public ChevroletCorsa(String color) {
		super(4, 150, 1300, color);
	}

}
