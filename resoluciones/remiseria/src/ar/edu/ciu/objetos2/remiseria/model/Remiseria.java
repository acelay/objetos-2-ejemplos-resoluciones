package ar.edu.ciu.objetos2.remiseria.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Remiseria {

	// atributos
	private Collection<Vehiculo> flota;
	private Collection<Viaje> viajesHechos;
	private Integer valorPorKilometro;
	private Integer valorMinimoPorViaje;

	// constructor
	public Remiseria() {
		super();
		this.flota = new ArrayList<Vehiculo>();
		this.viajesHechos = new ArrayList<Viaje>();
	}

	// metodos
	public void agregarAFlota(Vehiculo vehiculo) {
		this.flota.add(vehiculo);
	}

	public void quitarDeFlota(Vehiculo vehiculo) {
		this.flota.remove(vehiculo);
	}

	public Integer pesoTotalFlota() {
		return this.flota.stream().mapToInt(v -> v.getPeso()).sum();
		//return this.flota.stream().mapToInt(Vehiculo::getPeso).sum();
	}

	public Boolean esRecomendable() {
		return this.cantidadDeVehiculos()>=3 && !this.hayVehiculosQueNoSuperenLos(100);
	}

	public Integer capacidadTotalYendoA(Integer velocidad) {
		return this.flota.stream()
						.filter(v -> v.getVelocidadMaxima()>=velocidad)
						.mapToInt(Vehiculo::getCapacidad)
						.sum();
	}

	public Vehiculo vehiculoMasRapido() {
//		Comparator<Vehiculo> c = new Comparator<Vehiculo>() {
//			@Override
//			public int compare(Vehiculo arg0, Vehiculo arg1) {
//				return arg0.getVelocidadMaxima().compareTo(arg1.getVelocidadMaxima());
//			}
//		};
		return this.flota.stream()
				//.max(c)
				.max(Comparator.comparing(Vehiculo::getVelocidadMaxima))
				.get();
	}

	public void registrarViaje(Viaje viaje, Vehiculo vehiculo) {
		if (viaje.puedeHacerViaje(vehiculo)) {
			viaje.asignar(vehiculo);
			this.viajesHechos.add(viaje);
		}
	}

	private Integer cantidadDeVehiculos() {
		return this.flota.size();
	}

	private Boolean hayVehiculosQueNoSuperenLos(Integer velocidad) {
		List<Vehiculo> vehiculosLentos = this.flota.stream()
														.filter(v -> v.getVelocidadMaxima()<velocidad)
														.collect(Collectors.toList());
		return vehiculosLentos!=null && vehiculosLentos.size()>0;
	}

	public Integer cantidadDeViajesHechos() {
		return this.viajesHechos.size();
	}

	public Long cuantosViajesHizo(Vehiculo vehiculo) {
		return this.viajesHechos.stream()
									.filter(v -> v.getVehiculo()==vehiculo)
									.count();
	}

	public Long cuantosViajesHizoDeMasDe(Integer kilometros) {
		return this.viajesHechos.stream()
									.filter(v -> v.getKilometros()>kilometros)
									.count();
	}

	public Integer cantidadDeLugaresLibres() {
		return this.viajesHechos.stream()
									.mapToInt(Viaje::lugaresLibres)
									.sum();
	}

	public void asignarValores(Integer valorPorKilometro, Integer valorMinimo) {
		this.valorPorKilometro = valorPorKilometro;
		this.valorMinimoPorViaje = valorMinimo;
	}

	public Integer cuantoPagarleA(Vehiculo vehiculo) {
		return this.viajesHechos.stream()
				.filter(v -> v.getVehiculo()==vehiculo)
				.mapToInt(v -> v.valorDelViaje(this))
				.sum();
	}

	public Integer getValorPorKilometro() {
		return valorPorKilometro;
	}

	public Integer getValorMinimoPorViaje() {
		return valorMinimoPorViaje;
	}

}
