package ar.edu.ciu.objetos2.remiseria.model;

import java.util.Arrays;
import java.util.Collection;

public final class Viaje {

	// atributos
	private Integer kilometros;
	private Integer tiempoMaximo;
	private Integer pasajeros;
	private Collection<String> colores;
	private Vehiculo vehiculo;

	// constructor
	public Viaje(Integer kilometros, Integer tiempoMaximo, Integer pasajeros, String...colores) {
		super();
		this.kilometros = kilometros;
		this.tiempoMaximo = tiempoMaximo;
		this.pasajeros = pasajeros;
		this.colores = Arrays.asList(colores);
	}

	// metodos
	public Boolean puedeHacerViaje(Vehiculo vehiculo) {
		return 
				(vehiculo.getVelocidadMaxima() >= (this.velocidadPromedio()+10)) && 
				vehiculo.getCapacidad() >= this.pasajeros && 
				!this.hayColorIncompatible(vehiculo);
	}

	private boolean hayColorIncompatible(Vehiculo vehiculo) {
		return this.colores.stream().anyMatch(c -> c.equals(vehiculo.getColor()));
	}

	private Integer velocidadPromedio() {
		return (this.kilometros/this.tiempoMaximo);
	}

	public void asignar(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public Integer lugaresLibres() {
		return this.vehiculo.getCapacidad()-this.getPasajeros();
	}

	public Integer valorDelViaje(Remiseria remiseria) {
		Integer valor = this.kilometros*remiseria.getValorPorKilometro();
		return (valor<remiseria.getValorMinimoPorViaje())?remiseria.getValorMinimoPorViaje():valor;
	}

	// gets
	public Vehiculo getVehiculo() {
		return vehiculo;
	}

	public Integer getKilometros() {
		return kilometros;
	}

	public Integer getTiempoMaximo() {
		return tiempoMaximo;
	}

	public Integer getPasajeros() {
		return pasajeros;
	}

	public Collection<String> getColores() {
		return colores;
	}

}
