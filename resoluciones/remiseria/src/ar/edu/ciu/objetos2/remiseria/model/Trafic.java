package ar.edu.ciu.objetos2.remiseria.model;

import ar.edu.ciu.objetos2.remiseria.model.interior.Interior;
import ar.edu.ciu.objetos2.remiseria.model.motor.Motor;

// singleton
public class Trafic extends Vehiculo {

	// atributos estaticos
	private static Trafic instance;

	// atributos
	private Motor motor;
	private Interior interior;

	// metodos estaticos
	public static Trafic getIntance() {
		if (instance==null) {
			instance = new Trafic();
		}
		return instance;
	}

	// constructor
	private Trafic() {
		super();
		this.peso = 4000;
		this.color = "blanco";
	}

	// metodos

	public void configurar(Motor motor) {
		this.motor = motor;
	}

	public void configurar(Interior interior) {
		this.interior = interior;
	}

	@Override
	public Integer getCapacidad() {
		return this.interior.getCapacidad();
	}

	@Override
	public Integer getVelocidadMaxima() {
		return this.motor.getVelocidadMaxima();
	}

	@Override
	public Integer getPeso() {
		return this.peso + this.interior.getPeso() + this.motor.getPeso();
	}

	@Override
	public String getColor() {
		return this.color;
	}

}
