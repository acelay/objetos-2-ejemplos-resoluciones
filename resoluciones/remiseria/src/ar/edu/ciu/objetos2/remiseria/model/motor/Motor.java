package ar.edu.ciu.objetos2.remiseria.model.motor;

public abstract class Motor {

	// atributos
	protected Integer velocidadMaxima;
	protected Integer peso;

	// constructor
	public Motor(Integer velocidadMaxima, Integer peso) {
		super();
		this.velocidadMaxima = velocidadMaxima;
		this.peso = peso;
	}

	// metodos
	public Integer getVelocidadMaxima() {
		return velocidadMaxima;
	}

	public Integer getPeso() {
		return peso;
	}

}
