package ar.edu.ciu.objetos2.remiseria.test;

import ar.edu.ciu.objetos2.remiseria.model.ChevroletCorsa;
import ar.edu.ciu.objetos2.remiseria.model.Remiseria;
import ar.edu.ciu.objetos2.remiseria.model.Standard;
import ar.edu.ciu.objetos2.remiseria.model.Vehiculo;
import junit.framework.TestCase;

// Add library JUnit3 a classpath
public class General extends TestCase {

	public void testUno() {
		Vehiculo tito = new ChevroletCorsa("rojo");
		Vehiculo marta = new Standard(4, 90, 1000, "rojo");
		Remiseria cieloAzul = new Remiseria();
		cieloAzul.agregarAFlota(tito);
		cieloAzul.agregarAFlota(marta);
		assertEquals(Integer.valueOf(2300), cieloAzul.pesoTotalFlota());
	}

}
