package ar.edu.ciu.objetos2.remiseria.model;

public abstract class Vehiculo {

	// atributos
	protected Integer capacidad;
	protected Integer velocidadMaxima;
	protected Integer peso;
	protected String color;
	
	// metodos abstractos
	public abstract Integer getCapacidad();

	public abstract Integer getVelocidadMaxima();
	
	public abstract Integer getPeso();
	
	public abstract String getColor();

}
