package ar.edu.ciu.objetos2.remiseria.test;

import ar.edu.ciu.objetos2.remiseria.model.ChevroletCorsa;
import ar.edu.ciu.objetos2.remiseria.model.Remiseria;
import ar.edu.ciu.objetos2.remiseria.model.Standard;
import ar.edu.ciu.objetos2.remiseria.model.StandardGas;
import ar.edu.ciu.objetos2.remiseria.model.Trafic;
import ar.edu.ciu.objetos2.remiseria.model.Vehiculo;
import ar.edu.ciu.objetos2.remiseria.model.Viaje;
import ar.edu.ciu.objetos2.remiseria.model.interior.Comodo;
import ar.edu.ciu.objetos2.remiseria.model.motor.Bataton;
import junit.framework.TestCase;


public class Tests extends TestCase {

	public void testItems() {
		Remiseria remiseria1 = new Remiseria();
		Vehiculo cachito = new ChevroletCorsa("rojo");
		Vehiculo corsaNegro = new ChevroletCorsa("negro");
		Vehiculo corsaVerde = new ChevroletCorsa("verde");
		Vehiculo standardGas = new StandardGas(true);
		Vehiculo distinto = new Standard(5, 160, 1200, "beige");
		remiseria1.agregarAFlota(cachito);
		remiseria1.agregarAFlota(corsaNegro);
		remiseria1.agregarAFlota(corsaVerde);
		remiseria1.agregarAFlota(standardGas);
		remiseria1.agregarAFlota(distinto);
		
		Remiseria remiseria2 = new Remiseria();
		Vehiculo standard1 = new StandardGas(true);
		Vehiculo standard2 = new StandardGas(false);
		Vehiculo standard3 = new StandardGas(false);
		Trafic trafic = Trafic.getIntance();
		trafic.configurar(new Comodo());
		trafic.configurar(new Bataton());
		remiseria2.agregarAFlota(cachito);
		remiseria2.agregarAFlota(standard1);
		remiseria2.agregarAFlota(standard2);
		remiseria2.agregarAFlota(standard3);
		remiseria2.agregarAFlota(trafic);

		// tests itemB
		assertEquals(Integer.valueOf(6450), remiseria1.pesoTotalFlota());
		
		assertTrue(remiseria1.esRecomendable());
		assertFalse(remiseria2.esRecomendable()); // la trafic va menos de 100
		
		assertEquals(Integer.valueOf(17), remiseria1.capacidadTotalYendoA(Integer.valueOf(140)));
		assertEquals(Integer.valueOf(4), remiseria2.capacidadTotalYendoA(Integer.valueOf(140)));

		assertEquals(Integer.valueOf(17), remiseria1.capacidadTotalYendoA(Integer.valueOf(140)));
		assertEquals(Integer.valueOf(4), remiseria2.capacidadTotalYendoA(Integer.valueOf(140)));

		assertEquals(distinto, remiseria1.vehiculoMasRapido());
		assertEquals(cachito, remiseria2.vehiculoMasRapido());

		// tests itemC
		Viaje viajeADevoto = new Viaje(170, 3, 2, new String[] {"amarillo", "verde", "celeste"});
		Viaje viajeAVillanueva = new Viaje(40, 2, 1, new String[] {});
		assertTrue(viajeADevoto.puedeHacerViaje(cachito));
		assertFalse(viajeADevoto.puedeHacerViaje(corsaVerde)); // no machea el color

		// tests itemD
		remiseria1.registrarViaje(viajeADevoto, cachito);
		remiseria1.registrarViaje(viajeAVillanueva, corsaNegro);
		assertEquals(Integer.valueOf(2), remiseria1.cantidadDeViajesHechos());
		
		assertEquals(Long.valueOf(1), remiseria1.cuantosViajesHizoDeMasDe(100));
		assertEquals(Long.valueOf(2), remiseria1.cuantosViajesHizoDeMasDe(10));
		
		assertEquals(Integer.valueOf(5), remiseria1.cantidadDeLugaresLibres());
		
		remiseria1.asignarValores(3, 150);
		assertEquals(Integer.valueOf(510), remiseria1.cuantoPagarleA(cachito));
		assertEquals(Integer.valueOf(150), remiseria1.cuantoPagarleA(corsaNegro));
	}

}
