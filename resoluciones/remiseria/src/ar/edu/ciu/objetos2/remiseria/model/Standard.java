package ar.edu.ciu.objetos2.remiseria.model;

public class Standard extends Vehiculo {

	// constructor
	public Standard(Integer capacidad, Integer velocidadMaxima, Integer peso, String color) {
		super();
		this.capacidad = capacidad;
		this.velocidadMaxima = velocidadMaxima;
		this.peso = peso;
		this.color = color;
	}

	@Override
	public Integer getCapacidad() {
		return this.capacidad;
	}

	@Override
	public Integer getVelocidadMaxima() {
		return this.velocidadMaxima;
	}

	@Override
	public Integer getPeso() {
		return this.peso;
	}

	@Override
	public String getColor() {
		return this.color;
	}

}
