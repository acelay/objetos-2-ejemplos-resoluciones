package ar.edu.ciu.objetos2.examenes.model;

import java.util.ArrayList;
import java.util.List;

public class PreguntaMultipleChoice extends Pregunta {

	private String opcionCorrecta;
	private List<String> opciones;

	public PreguntaMultipleChoice(int puntajeMaximo, String opcionCorrecta) {
		super(puntajeMaximo);
		this.opcionCorrecta = opcionCorrecta;
		this.opciones = new ArrayList<String>();
	}

	public void addOpcion(String opcion) {
		this.opciones.add(opcion);
	}

	public String getOpcionCorrecta() { 
		return opcionCorrecta; 
	}

}
