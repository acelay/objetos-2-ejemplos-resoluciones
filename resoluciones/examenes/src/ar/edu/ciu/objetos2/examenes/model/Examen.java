package ar.edu.ciu.objetos2.examenes.model;

import java.util.ArrayList;
import java.util.List;

public class Examen {

	private List<Pregunta> preguntas;
	private int puntajeMinimoAprobacion;

	public Examen() {
		super();
		this.preguntas = new ArrayList<Pregunta>();
	}

	public void addToPreguntas(Pregunta preg) { 
		this.preguntas.add(preg); 
	}

	public int getPuntajeMaximo() {
		return this.preguntas.stream()
								.mapToInt((preg) -> preg.getPuntajeMaximo())
								.sum();
	}

	public List<Pregunta> getPreguntas() { 
		return preguntas;
	}	
	
	public int getPuntajeMinimoAprobacion() { 
		return this.puntajeMinimoAprobacion; 
	}

	public void setPuntajeMinimoAprobacion(int puntaje) { 
		this.puntajeMinimoAprobacion = puntaje; 
	}

	public boolean estaParaAprobar(ResolucionDeExamen resolucion) {
		return resolucion.getPuntajeObtenido() >= this.getPuntajeMinimoAprobacion();
	}

}
