package ar.edu.ciu.objetos2.examenes.model;

public abstract class Pregunta {

	private int puntajeMaximo;
	
	public Pregunta(int puntajeMaximo) { 
		this.puntajeMaximo = puntajeMaximo; 
	}
	
	public int getPuntajeMaximo() { 
		return this.puntajeMaximo; 
	}

}
