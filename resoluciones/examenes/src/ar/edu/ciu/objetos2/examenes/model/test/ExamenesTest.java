package ar.edu.ciu.objetos2.examenes.model.test;

import java.util.Arrays;

import ar.edu.ciu.objetos2.examenes.model.Examen;
import ar.edu.ciu.objetos2.examenes.model.PreguntaMultipleChoice;
import ar.edu.ciu.objetos2.examenes.model.PreguntaNumerica;
import ar.edu.ciu.objetos2.examenes.model.PreguntaSecuencia;
import ar.edu.ciu.objetos2.examenes.model.ResolucionDeExamen;
import ar.edu.ciu.objetos2.examenes.model.RespuestaMultipleChoice;
import ar.edu.ciu.objetos2.examenes.model.RespuestaNumerica;
import ar.edu.ciu.objetos2.examenes.model.RespuestaSecuencia;
import junit.framework.TestCase;

public class ExamenesTest extends TestCase {

	// Test basado en examen de ejemplo de ayudante de cocina
	public void testParte1Item1() {
		Examen examenAyudanteDeCocina = new Examen();

		PreguntaMultipleChoice preguntaMultipleChoice1 = new PreguntaMultipleChoice(20, "b");
		
		String arr[] = {"2","1","3","4","9","5","7","8","6"};
		PreguntaSecuencia preguntaSecuencia = new PreguntaSecuencia(20, Arrays.asList(arr));

		PreguntaNumerica preguntaNumerica = new PreguntaNumerica(20, 300);
		preguntaNumerica.setRangoAproximado(280, 320, 10);
		
		PreguntaMultipleChoice preguntaMultipleChoice2 = new PreguntaMultipleChoice(20, "a");

		PreguntaMultipleChoice preguntaMultipleChoice3 = new PreguntaMultipleChoice(20, "a");

		examenAyudanteDeCocina.addToPreguntas(preguntaMultipleChoice1);
		examenAyudanteDeCocina.addToPreguntas(preguntaSecuencia);
		examenAyudanteDeCocina.addToPreguntas(preguntaNumerica);
		examenAyudanteDeCocina.addToPreguntas(preguntaMultipleChoice2);
		examenAyudanteDeCocina.addToPreguntas(preguntaMultipleChoice3);
		
		assertEquals(100, examenAyudanteDeCocina.getPuntajeMaximo());
	}

	public void testParte1Item2() {
		// examen
		Examen examenAyudanteDeCocina = new Examen();

		PreguntaMultipleChoice preguntaMultipleChoice1 = new PreguntaMultipleChoice(20, "b");
		
		String arr[] = {"2","1","3","4","9","5","7","8","6"};
		PreguntaSecuencia preguntaSecuencia = new PreguntaSecuencia(20, Arrays.asList(arr));

		PreguntaNumerica preguntaNumerica = new PreguntaNumerica(20, 300);
		preguntaNumerica.setRangoAproximado(280, 320, 10);
		
		PreguntaMultipleChoice preguntaMultipleChoice2 = new PreguntaMultipleChoice(20, "a");

		PreguntaMultipleChoice preguntaMultipleChoice3 = new PreguntaMultipleChoice(20, "a");

		examenAyudanteDeCocina.addToPreguntas(preguntaMultipleChoice1);
		examenAyudanteDeCocina.addToPreguntas(preguntaSecuencia);
		examenAyudanteDeCocina.addToPreguntas(preguntaNumerica);
		examenAyudanteDeCocina.addToPreguntas(preguntaMultipleChoice2);
		examenAyudanteDeCocina.addToPreguntas(preguntaMultipleChoice3);

		
		// resolucion
		ResolucionDeExamen resolucionJaimito = new ResolucionDeExamen(examenAyudanteDeCocina);

		RespuestaMultipleChoice respuestaPreguntaMultiplChoice1 = new RespuestaMultipleChoice(preguntaMultipleChoice1);
		respuestaPreguntaMultiplChoice1.setValor("b");
		resolucionJaimito.addToRespuestas(respuestaPreguntaMultiplChoice1);

		RespuestaSecuencia respuestaPreguntaSecuencia = new RespuestaSecuencia(preguntaSecuencia);
		String secuencia = "1-2-3-4-9-5-7-8-6";
		respuestaPreguntaSecuencia.setValor(secuencia);
		resolucionJaimito.addToRespuestas(respuestaPreguntaSecuencia);

		RespuestaNumerica respuestaNumerica = new RespuestaNumerica(preguntaNumerica);
		respuestaNumerica.setValor("300");
		resolucionJaimito.addToRespuestas(respuestaNumerica);
		
		RespuestaMultipleChoice respuestaPreguntaMultiplChoice2 = new RespuestaMultipleChoice(preguntaMultipleChoice2);
		respuestaPreguntaMultiplChoice2.setValor("a");
		resolucionJaimito.addToRespuestas(respuestaPreguntaMultiplChoice2);

		RespuestaMultipleChoice respuestaPreguntaMultiplChoice3 = new RespuestaMultipleChoice(preguntaMultipleChoice3);
		respuestaPreguntaMultiplChoice3.setValor("a");
		resolucionJaimito.addToRespuestas(respuestaPreguntaMultiplChoice3);

		assertEquals(80, resolucionJaimito.getPuntajeObtenido());
	}

}
