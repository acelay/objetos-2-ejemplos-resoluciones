package ar.edu.ciu.objetos2.examenes.model;

public class RespuestaNumerica extends RespuestaAPregunta {

	private int valor;

	public RespuestaNumerica(PreguntaNumerica preg) { 
		super(preg); 
	}

	@Override
	public boolean esCorrecta() { 
		return this.getValor() == ((PreguntaNumerica)this.getPregunta()).getRespuestaCorrecta(); 
	}
	
	public boolean estaEnRango() { 
		return ((PreguntaNumerica)this.getPregunta()).getRangoAproximado().isEnRango(this.getValor()); 
	}

	@Override
	public int getPuntajeObtenido() {
		if (this.estaEnRango() && !this.esCorrecta()) {
			return ((PreguntaNumerica)this.getPregunta()).getPuntajeAproximado();
		} else {
			return super.getPuntajeObtenido();
		}
	}

	public int getValor() { 
		return this.valor; 
	}
	
	@Override
	public void setValor(String valor) { 
		this.valor = Integer.valueOf(valor); 
	}

}
