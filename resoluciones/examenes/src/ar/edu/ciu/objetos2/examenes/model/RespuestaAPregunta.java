package ar.edu.ciu.objetos2.examenes.model;

public abstract class RespuestaAPregunta {

	private Pregunta pregunta;
	
	public RespuestaAPregunta(Pregunta preg) {
		super();
		this.pregunta = preg;
	}
	
	public Pregunta getPregunta() { 
		return this.pregunta; 
	}
	
	public int getPuntajeObtenido() { 
		return this.esCorrecta() ? this.getPregunta().getPuntajeMaximo() : 0; 
	}

	public abstract boolean esCorrecta();

	public abstract void setValor(String valor);

}
