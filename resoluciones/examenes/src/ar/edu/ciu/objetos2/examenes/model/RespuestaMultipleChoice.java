package ar.edu.ciu.objetos2.examenes.model;

public class RespuestaMultipleChoice extends RespuestaAPregunta {

	public RespuestaMultipleChoice(PreguntaMultipleChoice preg) { 
		super(preg);
	}

	private String opcionElegida;

	@Override
	public boolean esCorrecta() {
		// Sin Generics en principio tengo que castear
		return this.opcionElegida.equals(((PreguntaMultipleChoice)this.getPregunta()).getOpcionCorrecta());
	}

	public String getOpcionElegida() { 
		return this.opcionElegida; 
	}

	@Override
	public void setValor(String valor) { 
		this.opcionElegida = valor; 
	}

}
