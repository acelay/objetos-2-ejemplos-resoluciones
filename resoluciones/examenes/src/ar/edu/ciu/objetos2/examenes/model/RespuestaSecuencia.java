package ar.edu.ciu.objetos2.examenes.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RespuestaSecuencia extends RespuestaAPregunta {
	
	private List<String> secuenciaElegida;
	
	public RespuestaSecuencia(PreguntaSecuencia preg) { 
		super(preg); 
	}

	@Override
	public boolean esCorrecta() {
		return this.getSecuenciaElegida().equals(((PreguntaSecuencia)this.getPregunta()).getSecuenciaCorrecta())
				|| 
				this.getSecuenciaElegidaAlReves().equals(((PreguntaSecuencia)this.getPregunta()).getSecuenciaCorrecta());
	}

	@Override
	public void setValor(String valor) { 
		this.secuenciaElegida = Arrays.asList(valor.split("-")); 
	}
	
	public List<String> getSecuenciaElegida() { 
		return this.secuenciaElegida; 
	}
	
	public List<String> getSecuenciaElegidaAlReves() { 
		List<String> nuevaSecuencia = new ArrayList<>(this.secuenciaElegida);
		Collections.reverse(nuevaSecuencia);
		return nuevaSecuencia;
	}

}
