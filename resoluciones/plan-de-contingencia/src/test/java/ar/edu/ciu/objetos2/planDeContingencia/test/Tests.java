package ar.edu.ciu.objetos2.planDeContingencia.test;

import ar.edu.ciu.objetos2.planDeContingencia.model.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Tests {

    @Test
    public void operacional1() {
        Empleado trotsky = new Empleado("Trotsky");
        Empleado marx = new Empleado("Marx");
        Empleado engels = new Empleado("Engels");
        Departamento marketing = new Operativo("Marketing", 40);
        marketing.agregar(trotsky);
        marketing.agregar(marx);
        marketing.agregar(engels);
        // evaluo cantidad de empleados funcionando normalmente
        Assertions.assertEquals(3, marketing.cantidadDeEmpeadosTrabajando());
        // evaluo cantidad de empleados funcionando en contingencia
        marketing.setFuncionalidad(Contingencia.getInstance());
        Assertions.assertEquals(1, marketing.cantidadDeEmpeadosTrabajando());
    }

    @Test
    public void gerencial1() {
        Empleado trotsky = new Empleado("Trotsky");
        Empleado marx = new Empleado("Marx");
        Empleado engels = new Empleado("Engels");
        Departamento marketing = new Operativo("Marketing", 40);
        marketing.agregar(trotsky);
        marketing.agregar(marx);
        marketing.agregar(engels);

        Empleado moraglio = new Empleado("Moraglio");
        Empleado zurlo = new Empleado("Zurlo");
        Empleado flegenal = new Empleado("Flegenal");
        Empleado gigliotti = new Empleado("Gigliotti");
        Empleado mandarino = new Empleado("Mandarino");
        Empleado bojanich = new Empleado("Bojanich");
        Empleado saracini = new Empleado("Saracini");
        Empleado gimenez = new Empleado("Gimenez");
        Empleado billordo = new Empleado("Billordo");
        Empleado pascuale = new Empleado("Pascuale");
        Departamento ventas = new Operativo("Ventas", 80);
        ventas.agregar(moraglio);
        ventas.agregar(zurlo);
        ventas.agregar(flegenal);
        ventas.agregar(gigliotti);
        ventas.agregar(mandarino);
        ventas.agregar(bojanich);
        ventas.agregar(saracini);
        ventas.agregar(gimenez);
        ventas.agregar(billordo);
        ventas.agregar(pascuale);

        Empleado lenin = new Empleado("Lenin");
        Empleado smith = new Empleado("Smith");
        Empleado kant = new Empleado("Kant");
        Empleado hegel = new Empleado("Hegel");
        Empleado hayek = new Empleado("Hayek");
        Empleado keynes = new Empleado("Keynes");
        Gerencial comercial = new Gerencial("Comercial", 50);
        comercial.agregar(lenin);
        comercial.agregar(smith);
        comercial.agregar(kant);
        comercial.agregar(hegel);
        comercial.agregar(hayek);
        comercial.agregar(keynes);
        comercial.agregar(ventas);
        comercial.agregar(marketing);

        Assertions.assertEquals(19, comercial.cantidadDeEmpeadosTrabajando());

        comercial.setFuncionalidad(Contingencia.getInstance());
        Assertions.assertEquals(16, comercial.cantidadDeEmpeadosTrabajando());
    }

}
