package ar.edu.ciu.objetos2.planDeContingencia.model;

public class Empleado {

    public String nombre;

    public Empleado(String nombre) {
        super();
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

}
