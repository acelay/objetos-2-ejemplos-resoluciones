package ar.edu.ciu.objetos2.planDeContingencia.model;

import java.util.ArrayList;
import java.util.List;

public abstract class Departamento {

    protected String descripcion;
    protected List<Empleado> empleados;
    protected Funcionalidad funcionalidad;
    protected Integer porcentajeMinimoDeEmpleados;

    public Departamento(String descripcion, Integer porcentajeMinimoDeEmpleados) {
        super();
        this.descripcion = descripcion;
        this.porcentajeMinimoDeEmpleados = porcentajeMinimoDeEmpleados;
        this.empleados = new ArrayList<>();
        this.funcionalidad = Normal.getInstance();
    }

    public abstract Integer cantidadDeEmpeadosTrabajando();

    public void agregar(Empleado empleado) {
        this.empleados.add(empleado);
    }

    public void setFuncionalidad(Funcionalidad funcionalidad) {
        this.funcionalidad = funcionalidad;
    }

    public Integer cantidadDeEmpeados() {
        return this.empleados.size();
    }

}
