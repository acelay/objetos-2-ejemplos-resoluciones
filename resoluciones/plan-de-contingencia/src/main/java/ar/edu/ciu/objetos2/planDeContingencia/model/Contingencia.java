package ar.edu.ciu.objetos2.planDeContingencia.model;

/*
 *  Singleton
 */
public final class Contingencia extends Funcionalidad {

    private static Funcionalidad instance;

    private Contingencia() {
        super();
    }

    public static Funcionalidad getInstance() {
        if (instance==null)
            instance = new Contingencia();
        return instance;
    }

    @Override
    public Integer calcularCantidadDeEmpeadosTrabajando(Departamento departamento) {
        return (int)(departamento.cantidadDeEmpeados()*departamento.porcentajeMinimoDeEmpleados ) / 100;
    }

}
