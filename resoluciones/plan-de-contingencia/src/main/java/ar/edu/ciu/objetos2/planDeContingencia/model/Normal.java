package ar.edu.ciu.objetos2.planDeContingencia.model;

public final class Normal extends Funcionalidad {

    private static Funcionalidad instance;

    private Normal() {
        super();
    }

    public static Funcionalidad getInstance() {
        if (instance==null)
            instance = new Normal();
        return instance;
    }

    @Override
    public Integer calcularCantidadDeEmpeadosTrabajando(Departamento departamento) {
        return departamento.cantidadDeEmpeados();
    }

}
