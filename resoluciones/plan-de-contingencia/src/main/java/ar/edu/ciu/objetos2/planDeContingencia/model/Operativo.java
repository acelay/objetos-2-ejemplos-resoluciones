package ar.edu.ciu.objetos2.planDeContingencia.model;

public final class Operativo extends Departamento {

    public Operativo(String descripcion, Integer porcentajeMinimoDeEmpleados) {
        super(descripcion, porcentajeMinimoDeEmpleados);
    }

    @Override
    public Integer cantidadDeEmpeadosTrabajando() {
        return this.funcionalidad.calcularCantidadDeEmpeadosTrabajando(this);
    }

}
