package ar.edu.ciu.objetos2.planDeContingencia.model;

import java.util.ArrayList;
import java.util.List;

public class Empresa {

    private String descripcion;
    private List<Departamento> departamentos;

    public Empresa() {
        super();
        this.descripcion = "Cubito";
        this.departamentos = new ArrayList<>();
    }

    public void agregar(Departamento departamento) {
        this.departamentos.add(departamento);
    }

    public Integer cantidadDeEmpeadosTrabajando() {
        return this.departamentos.stream()
                                        .mapToInt(d -> d.cantidadDeEmpeadosTrabajando())
                                        .sum();
    }

}
