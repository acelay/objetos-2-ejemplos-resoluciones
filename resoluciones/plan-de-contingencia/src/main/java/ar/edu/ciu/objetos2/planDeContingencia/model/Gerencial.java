package ar.edu.ciu.objetos2.planDeContingencia.model;

import java.util.ArrayList;
import java.util.List;

public final class Gerencial extends Departamento {

    private List<Departamento> departamentos;

    public Gerencial(String descripcion, Integer porcentajeMinimoDeEmpleados) {
        super(descripcion, porcentajeMinimoDeEmpleados);
        this.departamentos = new ArrayList<>();
    }

    public void agregar(Departamento departamento) {
        this.departamentos.add(departamento);
    }

    @Override
    public Integer cantidadDeEmpeadosTrabajando() {
        Integer dependientes =
                    this.departamentos.stream()
                                        .mapToInt( d -> d.funcionalidad.calcularCantidadDeEmpeadosTrabajando(d))
                                        .sum();
        return dependientes + this.funcionalidad.calcularCantidadDeEmpeadosTrabajando(this);
    }

}
