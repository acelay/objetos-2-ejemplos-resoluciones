package ar.edu.ciu.objetos2.civilization.model.edificio;

public class EdificioEconomico extends Edificio {

	private Integer pepinesPorTurno;

	public EdificioEconomico(Integer costoConstruccion, Integer costoMantenimiento, Integer pepinesPorTurno) {
		super(costoConstruccion, costoMantenimiento);
		this.pepinesPorTurno = pepinesPorTurno;
	}

	@Override
	public Integer getCulturaIrradiada() {
		return (this.pepinesPorTurno<=500?2:3);
	}

	@Override
	public Integer getPepinesPorTurno() {
		return pepinesPorTurno;
	}

	@Override
	public Integer getTranquilidadAportada() {
		return 0;
	}

	@Override
	public Integer getValor() {
		return this.getPepinesPorTurno();
	}

}
