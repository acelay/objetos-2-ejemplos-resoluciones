package ar.edu.ciu.objetos2.civilization.model.edificio;

import ar.edu.ciu.objetos2.civilization.model.Juego;

public class EdificioCultural extends Edificio {

	private Integer culturaIrradiada;

	public EdificioCultural(Integer costoConstruccion, Integer costoMantenimiento, Integer culturaIrradiada) {
		super(costoConstruccion, costoMantenimiento);
		this.culturaIrradiada = culturaIrradiada;
	}

	@Override
	public Integer getCulturaIrradiada() {
		return this.culturaIrradiada;
	}

	@Override
	public Integer getTranquilidadAportada() {
		return this.culturaIrradiada / Juego.getInstance().getFactorTranquilidad();
	}

	@Override
	public Integer getValor() {
		return this.getCulturaIrradiada();
	}

}
