package ar.edu.ciu.objetos2.civilization.test;

import java.util.List;

import ar.edu.ciu.objetos2.civilization.model.Ciudad;
import ar.edu.ciu.objetos2.civilization.model.Imperio;
import ar.edu.ciu.objetos2.civilization.model.Tecnologia;
import ar.edu.ciu.objetos2.civilization.model.Tesoro;
import ar.edu.ciu.objetos2.civilization.model.edificio.Edificio;
import ar.edu.ciu.objetos2.civilization.model.edificio.EdificioCultural;
import ar.edu.ciu.objetos2.civilization.model.edificio.EdificioEconomico;
import ar.edu.ciu.objetos2.civilization.model.edificio.EdificioMilitar;
import ar.edu.ciu.objetos2.civilization.model.humor.Pacifista;
import ar.edu.ciu.objetos2.civilization.model.humor.Perseguida;
import junit.framework.TestCase;

public class Tests extends TestCase {

	public void testItem1() {
		Imperio azteca = new Imperio();
		Ciudad tenochtitlan = new Ciudad(16000);
		Edificio edTe1 = new EdificioCultural(8, 2, 5);
		Edificio edTe2 = new EdificioCultural(4, 1, 3);
		Edificio edTe3 = new EdificioEconomico(20, 8, 400);
		Edificio edTe4 = new EdificioMilitar(100, 20, 50);
		tenochtitlan.agregarEdificio(edTe1);
		tenochtitlan.agregarEdificio(edTe2);
		tenochtitlan.agregarEdificio(edTe3);
		tenochtitlan.agregarEdificio(edTe4);
		Ciudad aztlan = new Ciudad(12000);
		Edificio edAz1 = new EdificioCultural(8, 3, 6);
		aztlan.agregarEdificio(edAz1);
		azteca.agregarSede(tenochtitlan);
		azteca.agregarSede(aztlan);
		assertEquals(Integer.valueOf(10), tenochtitlan.getCulturaQueIrradianEdificios());
		assertEquals(Integer.valueOf(6), aztlan.getCulturaQueIrradianEdificios());
		List<Ciudad> ciudadesPorCultura = azteca.getCiudadesPorCultura();
		assertEquals(tenochtitlan, ciudadesPorCultura.get(0));
		assertEquals(aztlan, ciudadesPorCultura.get(1));
	}

	public void testItem2() {
		Ciudad tenochtitlan = new Ciudad(16000);
		Edificio edTe1 = new EdificioCultural(8, 2, 5);
		Edificio edTe2 = new EdificioCultural(4, 1, 3);
		Edificio edTe3 = new EdificioEconomico(20, 8, 400);
		Edificio edTe4 = new EdificioMilitar(100, 20, 50);
		tenochtitlan.agregarEdificio(edTe1);
		tenochtitlan.agregarEdificio(edTe2);
		tenochtitlan.agregarEdificio(edTe3);
		tenochtitlan.agregarEdificio(edTe4);
		Ciudad aztlan = new Ciudad(12000);
		Edificio edAz1 = new EdificioCultural(8, 3, 6);
		aztlan.agregarEdificio(edAz1);
		assertTrue(tenochtitlan.getEdificioMasValioso()==edTe3);
		assertTrue(aztlan.getEdificioMasValioso()==edAz1);
	}

	public void testItem3() {
		Ciudad tenochtitlan = new Ciudad(16000);
		Edificio edTe1 = new EdificioCultural(8, 2, 5);
		Edificio edTe2 = new EdificioCultural(4, 1, 3);
		Edificio edTe3 = new EdificioEconomico(20, 8, 400);
		Edificio edTe4 = new EdificioMilitar(100, 20, 50);
		tenochtitlan.agregarEdificio(edTe1);
		tenochtitlan.agregarEdificio(edTe2);
		tenochtitlan.agregarEdificio(edTe3);
		tenochtitlan.agregarEdificio(edTe4);
		tenochtitlan.setHumor(new Perseguida());
		Ciudad aztlan = new Ciudad(12000);
		Edificio edAz1 = new EdificioCultural(8, 3, 6);
		aztlan.agregarEdificio(edAz1);
		aztlan.setHumor(new Pacifista());
		assertTrue(tenochtitlan.esFeliz());
		assertTrue(aztlan.esFeliz());
	}

	public void testItem4() {
		Imperio azteca = new Imperio();
		Tecnologia rueda = new Tecnologia("rueda");
		Tecnologia fuego = new Tecnologia("fuego");
		Tecnologia martillo = new Tecnologia("martillo");
		Tecnologia fundicion = new Tecnologia("fundicion");
		fundicion.agregarRequisito(fuego);
		Tecnologia metal = new Tecnologia("metal");
		metal.agregarRequisito(fundicion);
		metal.agregarRequisito(martillo);
		Tecnologia tren = new Tecnologia("tren");
		tren.agregarRequisito(rueda);
		tren.agregarRequisito(metal);
		assertTrue(azteca.incorporarTecnologia(rueda)!=null);
		assertTrue(azteca.incorporarTecnologia(fundicion)==null); // no la puede incorporar, falta el fuego
		assertTrue(azteca.incorporarTecnologia(fuego)!=null);
		assertTrue(azteca.incorporarTecnologia(fundicion)!=null);
		assertTrue(azteca.incorporarTecnologia(metal)==null); // no la puede incorporar, falta el martillo
		assertTrue(azteca.incorporarTecnologia(martillo)!=null);
		assertTrue(azteca.incorporarTecnologia(metal)!=null);
		assertTrue(azteca.incorporarTecnologia(tren)!=null);
	}

	public void testItem5() {
		Imperio azteca = new Imperio();
		Ciudad tenochtitlan = new Ciudad(16000);
		Edificio edTe1 = new EdificioCultural(8, 2, 5);
		Edificio edTe2 = new EdificioCultural(4, 1, 3);
		Edificio edTe3 = new EdificioEconomico(20, 8, 400);
		Edificio edTe4 = new EdificioMilitar(100, 20, 50);
		Edificio edTe5 = new EdificioEconomico(15, 6, 200);
		tenochtitlan.agregarEdificio(edTe1);
		tenochtitlan.agregarEdificio(edTe2);
		tenochtitlan.agregarEdificio(edTe3);
		tenochtitlan.agregarEdificio(edTe4);
		tenochtitlan.agregarEdificio(edTe5);
		Ciudad aztlan = new Ciudad(12000);
		Edificio edAz1 = new EdificioCultural(8, 2, 6);
		Edificio edAz2 = new EdificioEconomico(5, 5, 50);
		aztlan.agregarEdificio(edAz1);
		aztlan.agregarEdificio(edAz2);
		Ciudad texcoco = new Ciudad(12000);
		Edificio edTex1 = new EdificioCultural(4, 1, 2);
		texcoco.agregarEdificio(edTex1);
		azteca.agregarSede(tenochtitlan);
		azteca.agregarSede(aztlan);
		azteca.agregarSede(texcoco);
		// subitem: a
		assertEquals(Integer.valueOf(650), azteca.ingresosPorTurno());
		// subitem: b
		assertEquals(Integer.valueOf(45), azteca.egresosPorTurno());
		// subitem: c
		assertEquals(Integer.valueOf(50), azteca.potenciaPorTurno());
	}

	public void testItem6() {
		Imperio azteca = new Imperio();
		azteca.setTesoro(new Tesoro(500000));
		Ciudad tenochtitlan = new Ciudad(16000);
		Edificio edTe1 = new EdificioCultural(8, 2, 5);
		Edificio edTe2 = new EdificioCultural(4, 1, 3);
		Edificio edTe3 = new EdificioEconomico(20, 8, 400);
		Edificio edTe4 = new EdificioMilitar(100, 20, 50);
		Edificio edTe5 = new EdificioEconomico(15, 6, 200);
		tenochtitlan.agregarEdificio(edTe1);
		tenochtitlan.agregarEdificio(edTe2);
		tenochtitlan.agregarEdificio(edTe3);
		tenochtitlan.agregarEdificio(edTe4);
		tenochtitlan.agregarEdificio(edTe5);
		tenochtitlan.setHumor(new Perseguida());
		Ciudad aztlan = new Ciudad(12000);
		Edificio edAz1 = new EdificioCultural(8, 2, 6);
		Edificio edAz2 = new EdificioEconomico(5, 5, 50);
		aztlan.agregarEdificio(edAz1);
		aztlan.agregarEdificio(edAz2);
		aztlan.setHumor(new Perseguida());
		Ciudad texcoco = new Ciudad(12000);
		Edificio edTex1 = new EdificioCultural(4, 1, 2);
		texcoco.agregarEdificio(edTex1);
		texcoco.setHumor(new Perseguida());
		azteca.agregarSede(tenochtitlan);
		azteca.agregarSede(aztlan);
		azteca.agregarSede(texcoco);
		assertTrue(tenochtitlan.esFeliz());
		assertTrue(aztlan.esFeliz());
		assertFalse(texcoco.esFeliz());
		azteca.evolucion();
		// subitem: a
		assertEquals(Integer.valueOf(16800), tenochtitlan.getHabitantes());
		assertEquals(Integer.valueOf(12600), aztlan.getHabitantes());
		assertEquals(Integer.valueOf(12000), texcoco.getHabitantes());
		// subitem: d
		assertEquals(Integer.valueOf(1), tenochtitlan.getCantidadDeUnidadesMilitares());
		assertEquals(Integer.valueOf(0), aztlan.getCantidadDeUnidadesMilitares());
		assertEquals(Integer.valueOf(0), texcoco.getCantidadDeUnidadesMilitares());
	}

}
