package ar.edu.ciu.objetos2.civilization.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ar.edu.ciu.objetos2.civilization.model.edificio.Edificio;
import ar.edu.ciu.objetos2.civilization.model.edificio.EdificioMilitar;
import ar.edu.ciu.objetos2.civilization.model.humor.Humor;

public class Ciudad {

	private List<Edificio> edificios;
	private List<UnidadMilitar> unidadesMilitares;
	private Integer habitantes;
	private Humor humor;

	public Ciudad(Integer habitantesIniciales) {
		super();
		this.edificios = new ArrayList<Edificio>();
		this.unidadesMilitares = new ArrayList<UnidadMilitar>();
		this.habitantes = habitantesIniciales;
	}

	public void agregarEdificio(Edificio edificio) {
		edificio.setCiudad(this);
		this.edificios.add(edificio);
	}

	public Integer getCulturaQueIrradianEdificios() {
		return this.edificios.stream()
								.mapToInt(Edificio::getCulturaIrradiada)
								.sum();
								
	}

	public void agregarUnidadMilitar(UnidadMilitar unidadMilitar) {
		this.unidadesMilitares.add(unidadMilitar);
	}

	public List<Edificio> getEdificios() {
		return edificios;
	}

	public List<UnidadMilitar> getUnidadesMilitares() {
		return unidadesMilitares;
	}

	public Integer getHabitantes() {
		return habitantes;
	}

	public Edificio getEdificioMasValioso() {
		return this.edificios.stream()
								.sorted(Comparator.comparing(Edificio::getValor).reversed())
								.findFirst()
								.get();
	}

	public Integer getCantidadDeUnidadesMilitares() {
		return this.unidadesMilitares.size();
	}

	public Integer getDisconformidad() {
		return this.getHumor().getDisconformidad(this);
	}

	public Integer getTranquilidad() {
		return this.edificios.stream()
								.mapToInt(Edificio::getTranquilidadAportada)
								.sum();
	}

	public boolean esFeliz() {
		return this.getTranquilidad() > this.getDisconformidad();
	}

	public Integer ingresosPorTurno() {
		return this.edificios.stream()
								.mapToInt(Edificio::getPepinesPorTurno)
								.sum();
	}

	public Integer egresosPorTurno() {
		return this.edificios.stream()
								.mapToInt(Edificio::getCostoMantenimiento)
								.sum();
	}

	public Integer potenciaPorTurno() {
		return this.edificios.stream()
								.mapToInt(Edificio::getPotencia)
								.sum();
	}

	public void aumentarPoblacion(Integer porcentaje) {
		this.habitantes = this.habitantes + (porcentaje * this.habitantes)/100;
	}

	public Integer getCostoMantenimientoEdificios() {
		return this.edificios.stream()
								.mapToInt(Edificio::getCostoMantenimiento)
								.sum();
	}

	public Integer getProduccionEdificios() {
		return this.edificios.stream()
								.mapToInt(Edificio::getPepinesPorTurno)
								.sum();
	}

	public void crearUnidadesMilitares() {
		this.edificios.stream()
							.filter(Edificio::esMilitar)
							.forEach(e -> ((EdificioMilitar)e).agregarUnidadMilitar());
	}

	public Humor getHumor() {
		return this.humor;
	}

	public void setHumor(Humor humor) {
		this.humor = humor;
	}

}
