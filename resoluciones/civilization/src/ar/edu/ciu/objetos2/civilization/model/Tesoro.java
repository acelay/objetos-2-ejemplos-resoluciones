package ar.edu.ciu.objetos2.civilization.model;

public class Tesoro {

	private Integer pepines;

	public Tesoro(Integer pepines) {
		super();
		this.pepines = pepines;
	}

	public void restarPepines(Integer cantidad) {
		this.pepines -= this.pepines;
	}

	public void sumarPepines(Integer produccionEdificios) {
		this.pepines += this.pepines;
	}

	public Integer getPepines() {
		return pepines;
	}

}
