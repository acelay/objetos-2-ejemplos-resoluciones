package ar.edu.ciu.objetos2.civilization.model.humor;

import ar.edu.ciu.objetos2.civilization.model.Ciudad;

public abstract class Humor {

	public abstract Integer disconformidad(Ciudad ciudad);

	public Integer getDisconformidad(Ciudad ciudad) {
		return this.disconformidad(ciudad);
	}

}
